#!/bin/bash

git checkout master
git tag -a release-$VERSION -m "$VERSION"
git push --tags